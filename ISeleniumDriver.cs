﻿using OpenQA.Selenium;
using System;
using OpenQA.Selenium.Support.UI;
using System.IO;

namespace Selenium
{
    public abstract class ISeleniumDriver
    {
        protected int _timeout;
        protected IWebDriver webDriver;

        protected static ISeleniumDriver Instance;

        protected abstract void _waitForReady();
        
        public static string getUrl()
        {
            return Instance.webDriver.Url;
        }

        public void Destroy()
        {
            webDriver.Dispose();
            webDriver = null;
        }

        public static void waitForReady()
        {
            Instance._waitForReady();
        }

        public static void navigate(string url)
        {
            Instance._waitForReady();
            Instance.webDriver.Navigate().GoToUrl(url);
        }

        public static void waitForElement(By element, bool displayed = true)
        {
            Instance._waitForReady();

            var wait = new WebDriverWait(Instance.webDriver, TimeSpan.FromSeconds(Instance._timeout));

            System.Threading.Thread.Sleep(500);

            wait.Until(drv => {
                try
                {
                    if (displayed)
                    {
                        return drv.FindElement(element).Displayed;
                    }
                    else
                    {
                        return drv.FindElement(element) != null;
                    }
                }
                catch (Exception)
                {
                    return false;
                }
            });
        }

        public static void sendKeys(By element, string keys, bool clear = false)
        {
            waitForElement(element);
            if (clear)
            {
                Instance.webDriver.FindElement(element).Clear();
                //System.Threading.Thread.Sleep(500);
            }
            Instance.webDriver.FindElement(element).SendKeys(keys);
        }

        public static void sendFile(By element, string fileName)
        {
            waitForElement(element, false);

            Instance.webDriver.FindElement(element).SendKeys(fileName);

            System.Threading.Thread.Sleep(1000);
            waitForReady();
        }

        public static bool checkElementExists(By element)
        {
            Instance._waitForReady();
            try
            {
                if (Instance.webDriver.FindElement(element).Displayed)
                {
                    return true;
                }
            }
            catch (NotFoundException e)
            {
                return false;
            }

            return false;
        }

        public static void checkAlert(string text = null)
        {
            Instance._waitForReady();
            bool result = true;

            for (int i = 0; i < 10; i++)
            {
                try
                {
                    IAlert alert = Instance.webDriver.SwitchTo().Alert();
                    if(text != null && alert.Text != text)
                    {
                        result = false;
                        break;
                    }

                    alert.Accept();
                    break;
                }
                catch (Exception e)
                {
                    System.Threading.Thread.Sleep(1000);
                }
            }

            if(!result)
            {
                throw new Exception("não foi encontrado o texto esperado no alert");
            }
            
            Instance._waitForReady();
        }

        public static void click(By element)
        {
            waitForElement(element);
            for(int i = 0; i < 10; i++)
            {
                try
                {
                    findElement(element).Click();
                    break;
                }
                catch (Exception e)
                {
                    System.Threading.Thread.Sleep(1000);
                }
            }

            Instance._waitForReady();
        }

        public static IWebElement findElement(By by)
        {
            return Instance.webDriver.FindElement(by);
        }

        public static string getText(By element)
        {
            waitForElement(element);
            return findElement(element).Text;
        }
    }
}
